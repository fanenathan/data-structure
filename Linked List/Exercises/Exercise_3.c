#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SIZE 20;

typedef struct Student {
    char *name;
    char *ID;
    int age;
    int score;

    struct Student *next;
    struct Student *prev;
} Student;

unsigned hashMap(char *ID) {
    unsigned int hashValue = 5381;
    int c;

    while ((c = getchar()) != '\n' && c != EOF) {
        // Update the hash value based on the current character
        hashValue = ((hashValue << 4) + c);
        // Ensure the hash value doesn't exceed the maximum for an unsigned int
        hashValue &= 0xFFFFFFFF;
        c = *ID++;
    }

    return hashValue % SIZE;
}

void flushBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

int isUnique(char *ID, Student **head) {
    Student *current = *head;
    while (current != NULL) {
        if (strcmp(current->ID, ID) == 0) {
            return 0;
        }
        current = current->next;
    }
    return 1;
}

int age() {
    int age;
    scanf("%d", &age);
    flushBufferInput();
    if (age <= 0) {
        printf("Age cannot be 0 or lower..... You must be kidding me\n");
    }
    return age;
}

double score() {
    double score;
    scanf("%lf", &score);
    flushBufferInput();
    if (score < 0) {
        printf("The score can't be negative... ain't now way the student got negative score\n");
    }
    return score;
}

char *randID(int isUnique()) {
    char *id = (char *)malloc(sizeof(char) * 10);

    do {
        id[0] = '2';
        id[1] = '7';
        id[2] = '0';

        for (int i = 3; i < 9; i++) {
            id[i] = rand() % 10 + '0';
        }

        id[9] = '\0';
    } while (!isUnique(id));

    return id;
}

char *name() {
    char *name = (char *)malloc(sizeof(char) * 20);
    if (name == NULL) {
        printf("Memory allocation failed\n");
        exit(1);
    }

    do {
        fgets(name, 20, stdin);
        name[strcspn(name, "\n")] = '\0';

        if (strlen(name) == 0) {
            printf("Name cannot be empty!\n");
        }
    } while (strlen(name) == 0);

    return name;
}

Student *createNode(char *name, char *randID, int age, double score) {
    Student *studentData = (Student *)malloc(sizeof(Student));

    if (studentData == NULL) {
        printf("Failed to create a node.... sorry 😔\n");
        return NULL;
    }

    studentData->name = name;
    studentData->ID = randID;
    studentData->age = age;
    studentData->score = score;

    studentData->next = NULL;
    studentData->prev = NULL;

    return studentData;
}

void insert(char *name, char *randID, int age, double score) {
    Student *insert = (Student*)malloc(sizeof(Student));


}

void Menu(Student **head, Student **tail, int userOpt) {
    switch (userOpt) {
        case 1:
            addStudents(head, tail);
            break;
        case 2:
            deleteStudent(head, tail);
            break;
        case 3:
            updateData(head, tail);
            break;
        case 4:
            searchStudent(head, tail);
            break;
        case 5:
            printf("Exiting menu...\n");
            break;
        default:
            printf("Invalid input! Please try again\n");
            break;
    }
}

int main(void) {
    Student *head = NULL;
    Student *tail = NULL;
    int userOpt;

    do {
#ifdef _WIN32
        system("cls");
#else
        system("clear");
#endif

        printf("====== Student's Data ======\n");
        printf(" 1. Add student data\n");
        printf(" 2. Delete student data\n");
        printf(" 3. Update student data\n");
        printf(" 4. Search student\n");
        printf(" 5. Exit Menu\n");
        printf("============================\n");
        printf("Option: ");
        scanf("%d", userOpt);
        flushBufferInput();

        Menu(&head, &tail, userOpt);
    } while (userOpt != 5);

    return 0;
}