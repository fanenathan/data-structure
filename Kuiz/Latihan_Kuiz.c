#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SIZE 50

typedef struct person {
    int age;
    char *name;
    char *NIM;
    char *gender;

    struct person *next;
    struct person *prev;
} person;

typedef struct Hashmap {
    person *slot[SIZE];
} Hashmap;

unsigned int Hashing(char *id) {
    unsigned int Hashval = 5381;
    int c;
    while ((c = *id++)) {
        Hashval = ((Hashval << 5) + c);
        Hashval &= 0xFFFFFFFF;
    }
    return Hashval % SIZE;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

int age() {
    int age;
    printf("Enter your age: ");
    scanf("%d", &age);
    clearBufferInput();

    return age;
}

char *name() {
    char *userName = (char *)malloc(sizeof(char) * 100);

    if (userName == NULL) {
        printf("Failed to allocate memory for user name\n");
        exit(EXIT_FAILURE);
    }

    size_t size = 0;
    int c;

    while ((c = getchar()) != '\n' && c != EOF) {
        char *name = (char *)realloc(userName, size + 2);
        if (name == NULL) {
            free(userName);
            printf("Failed to allocate memories for user name\n");
            exit(EXIT_FAILURE);
        }

        userName = name;
        userName[size++] = c;
    }

    if (userName != NULL) {
        userName[size] = '\0';
    }

    return userName;
}

void genderMenu(int option, person **data) {
    switch (option) {
        case 1:
            (*data)->gender = "Male";
            break;
        case 2:
            (*data)->gender = "Female";
            break;
        default:
            break;
    }
}

void gender(person *data) {
    char *gender = (char *)malloc(sizeof(char) * 7);
    if (gender == NULL) {
        printf("Failed to allocate memories for gender\n");
        exit(EXIT_FAILURE);
    }

    int option;
    data->gender = NULL;

    printf("\nWhat is your gender?\n");
    printf("--------------------\n");
    printf(" 1. Male\n");
    printf(" 2. Female\n");
    printf(">> ");
    do {
        scanf("%d", &option);
        clearBufferInput();
        genderMenu(option, &data);
    } while (data->gender == NULL);
}

int isUnique(int flag, char *userIDs[], char *userID) {
    for (int i = 0; i < flag; i++) {
        if (strcmp(userIDs[i], userID) == 0) return 0;
    }
    return 1;
}

char **userIDs = NULL;

char *NIM() {
    srand(time(NULL));

    int flag = 0;

    do {
        char *userID = (char *)malloc(sizeof(char) * 11);
        if (userID == NULL) {
            printf("Failed to allocate memories for user ID\n");
            exit(EXIT_FAILURE);
        }

        int ID = (rand() % 90000000) + 10000000;
        ID = ID * 10 + (rand() % 10);
        sprintf(userID, "%010d", ID);

        userIDs = (char **)realloc(userID, flag * (sizeof(char *)));
        if (userIDs == NULL) {
            printf("Failed to resize the memories for user ID\n");
            exit(EXIT_FAILURE);
        }
        userIDs[flag] = userID;

        flag++;

    } while (!isUnique(flag, userIDs, userIDs[flag - 1]));

    return userIDs[flag - 1];
}

person *create(person **first, person **last) {
    person *new = (person *)malloc(sizeof(person));
    if (new == NULL) {
        printf("Failed to allocate memories for linked list\n");
        exit(EXIT_FAILURE);
    }

    printf("Enter your name: ");
    new->name = name();
    new->age = age();
    new->NIM = NIM();
    gender(new);
    new->next = NULL;
    new->prev = NULL;

    return new;
}

void addPerson(person ***first, person ***last, Hashmap **table) {
    person *add = create(*first, *last);
    if (add == NULL) {
        printf("Failed to add person to linked list\n");
        exit(EXIT_FAILURE);
    }

    int index = Hashing(add->NIM);
    add->next = (*table)->slot[index];
    (*table)->slot[index] = add;
}

void deletePerson(Hashmap **table, char *id) {
    int index = Hashing(id);
    person *temp = (*table)->slot[index];
    person *prev = NULL;

    while (temp != NULL) {
        if (strcmp(temp->NIM, id) == 0) {
            if (prev == NULL) {
                (*table)->slot[index] = temp->next;
            } else {
                prev->next = temp->next;
            }

            free(temp->name);
            free(temp->gender);
            free(temp);

            printf("Person with ID: %s is already deleted\n", id);
            return;
        }

        prev = temp;
        temp = temp->next;
    }

    printf("The person [%s] you are searching for is not available.\n", id);
}

void display(Hashmap *hashmap) {
    int is_empty = 1;

    for (int i = 0; i < SIZE; i++) {
        person *current = hashmap->slot[i];

        if (current != NULL) {
            printf("Slot %d: ", i);
            is_empty = 0;
            while (current != NULL) {
                printf("Name: %s\nAge: %d\nNIM: %s\nGender: %s\n", current->name, current->age, current->NIM, current->gender);
                current = current->next;
            }
        }
        if (is_empty) {
            printf("Hashmap is empty!\n");
        }
    }
}

void menu(int option, person **first, person **last, Hashmap **table) {
    switch (option) {
        case 1:
            addPerson(&first, &last, table);
            break;
        case 2:
            // searachPerson(table);
            break;
        case 3:
            printf("Please enter person NIM: ");
            char NIM[11];
            scanf("%10s", NIM);
            clearBufferInput();
            deletePerson(table, NIM);
            break;
        default:
            break;
    }
}

int main(void) {
    int option;
    person *first = NULL;
    person *last = NULL;
    Hashmap *table = (Hashmap *)malloc(sizeof(Hashmap));
    if (table == NULL) {
        printf("Failed to allocate memory for hashmap\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < SIZE; i++) {
        table->slot[i] = NULL;
    }

    do {
#ifdef _WIN32
        // system("cls");
#else
        system("clear");
#endif

        display(table);
        printf("======= Menu =======\n");
        printf(" 1. Add person\n");
        printf(" 2. Search person\n");
        printf(" 3. Delete person\n");
        printf(" 4. Update data\n");
        printf(" 5. Exit\n");
        printf("====================\n");
        printf(" >> ");
        scanf("%d", &option);
        clearBufferInput();

        menu(option, &first, &last, &table);
    } while (option != 5);

    for (int i = 0; i < SIZE; i++) {
        person *current = table->slot[i];
        while (current != NULL) {
            person *next = current->next;
            free(current->name);
            free(current->gender);
            free(current->NIM);
            free(current);
            current = next;
        }
    }

    free(table);

    return 0;
}