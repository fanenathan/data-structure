#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define HASH_SIZE 30

typedef struct person {
    char *name;
    char *gender;
    int NIM;
    int age;

    struct person *next;
    struct person *prev;
} person;

typedef struct HashTable {
    person *slot[HASH_SIZE];
} HashTable;

unsigned int Hashmap(int NIM) {
    //  unsigned int HashVal = 5381;: This line initializes the hash value to a prime number. The choice of 5381 isn’t arbitrary. It’s often used as the initial value in hash functions based on the “DJB2” algorithm.
    unsigned int HashVal = 5381;
    //  int c;: This line declares an integer c which will be used to hold each character of the input.
    int c;

    //  while ((c = NIM++)): This loop continues as long as c, which is assigned the value of NIM before NIM is incremented, is not zero.
    while ((c = NIM++)) {s
        //      HashVal = ((HashVal << 5) + c);: This line updates the hash value. The << 5 operation is a bitwise shift to the left by 5 positions, which is equivalent to multiplying by 32. So this line is equivalent to HashVal = HashVal * 32 + c;. This is a common operation in hash functions to mix the bits of the hash value.
        HashVal = ((HashVal << 5) + c);
        //      HashVal &= 0xFFFFFFFF;: This line is a bitwise AND operation that ensures HashVal stays as a 32-bit integer.
        HashVal &= 0xFFFFFFFF;
    }

    return HashVal % HASH_SIZE;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

void mallocError(char *string) {
    if (string == NULL) {
        printf("Error: cannot create a memory for user input\n");
        exit(1);
    }
}

char *name() {
    int bufferSize = 128;
    char *buffer = (char *)malloc(sizeof(char) * bufferSize);
    mallocError(buffer);

    printf("Please enter your name: ");
    int c, i = 0;

    while ((c = getchar()) != '\n' && c != EOF) {
        buffer[i++] = c;
        if (i == bufferSize) {
            bufferSize *= 2;
            buffer = realloc(buffer, sizeof(char) * bufferSize);
            mallocError(buffer);
        }
    }
    buffer[i] = '\0';

    buffer = realloc(buffer, sizeof(char) * (strlen(buffer) + 1));
    mallocError(buffer);

    return buffer;
}

char *gender() {
    int userOpt;
    char *gender = (char *)malloc(sizeof(char) * 10);

    printf("Please select your gender: ");
    printf("1. Male\n");
    printf("2. Female\n");

    do {
        scanf("%d", &userOpt);
        switch (userOpt) {
            case 1:
                strcpy(gender, "Male");
                break;
            case 2:
                strcpy(gender, "Female");
                break;
            default:
                printf("Invalid input\n");
                break;
        }
    } while (userOpt > 2 || userOpt < 1);

    return gender;
}

int randomGenInt() {
    srand(time(NULL));
    int id_set[1000000] = {0};
    int id;

    do {
        id = rand() % 1000000;
    } while (id_set[id] != 0);

    id_set[id] = 1;
    return id;
}

int age() {
    int age;
    printf("Please enter your age: ");
    scanf("%d", &age);
    clearBufferInput();

    return age;
}

person *addPerson(person **head, person **tail) {
    person *create = (person *)malloc(sizeof(person));
    if (create == NULL) {
        printf("Failed to create a memory when adding the data\n");
        exit(1);
    }

    create->age = age();
    create->gender = gender();
    create->name = name();
    create->NIM = randomGenInt();

    create->next = NULL;
    create->prev = NULL;

    return     
}

void menu(int userOpt, person **head, person **tail, HashTable table) {
    switch (userOpt) {
        case 1:
            addPerson(&head, &tail);
            break;
        case 2:
            deletePerson(&head, &tail, table);
            break;
        case 3:
            searchPerson();
            break;
        case 4:
            printf("Exiting....\n");
            break;
        default:
            printf("Invalid option!\n");
            break;
    }
}

int main() {
    int userOpt;
    person *head = NULL;
    person *tail = NULL;
    HashTable *table;


    do {
#ifdef _WIN32
    system("cls");
#else
    system("clear");
#endif

        printf("======= Data List =======\n");
        printf("  1. Add persons\n");
        printf("  2. Delete person\n");
        printf("  3. Search person\n");
        printf("  4. Exit the program\n");
        printf("==========================\n");
        scanf("%d", &userOpt);
        clearBufferInput();

        menu(userOpt, &head, &tail, &table);

    } while (userOpt != 4);

    return 0;
}
