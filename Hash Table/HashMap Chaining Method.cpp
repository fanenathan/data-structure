#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TABLE_SIZE 20

typedef struct person {
    char *name;
    char *id;
    int age;
    char *gender;

    struct person *next;
    struct person *prev;
} person;

typedef struct HashTable {
    person *bucket[TABLE_SIZE];
} HashTable;

unsigned int hashMap(char *id) {
    unsigned int hashVal = 5381;
    int c;

    while ((c = *id++)) {
        hashVal = ((hashVal << 4) + c);
        hashVal &= 0xFFFFFFFF;
    }
    return hashVal % TABLE_SIZE;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

char *name() {
    char *name = (char *)malloc(sizeof(name) * 20);
    if (name == NULL) {
        printf("Failed to creating a memory size for user input\n");
        exit(1);
    }

    do {
        printf("Enter person's name [20 characters max]: ");
        fgets(name, 20, stdin);
        name[strcspn(name, "\n")] == '\0';

        if (strlen(name) == 0) {
            printf("Name can't be empty!\n");
        }
    } while (strlen(name) > 0 || strlen(name) <= 20);

    return name;
}

char *random_generated_id() {
    srand(time(NULL));

    static int counter = 0;

    char *id = (char *)malloc(sizeof(char) * 50);
    time_t t = time(NULL);

    sprintf(id, "%ld%d", (long)t, rand() + counter++);
    return id;
}

int age() {
    int age;
    printf("Enter person's age: ");
    scanf("%d", &age);
    clearBufferInput();
    return age;
}

char *gender() {
    char *gender = (char *)malloc(sizeof(char) * 5);
    if (gender == NULL) {
        printf("Failed to create a memory for user input\n");
        exit(1);
    }

    do {
        printf("Enter person's gender: ");
        fgets(gender, 5, stdin);
        gender[strcspn(gender, "\n")] == '\0';

        if (strlen(gender) == 0) {
            printf("......the person doesn't have a gender?\n");
        }
    } while (strlen(gender) > 0 || strlen(gender) <= 5);

    return gender;
}

void addData(HashTable *table) {
    char *id = random_generated_id();

    unsigned int index = hashMap(id);

    person *data = (person *)malloc(sizeof(person));
    data->id = random_generated_id();
    data->age = age();
    data->gender = gender();
    data->name = name();

    data->next = NULL;
    data->prev = NULL;

    if (table->bucket[index] == NULL) {
        table->bucket[index] = data;
    } else {
        data->next = table->bucket[index];
        table->bucket[index] = data;
    }
}

void deleteData(HashTable *table, char *id) {
    unsigned int index = hashMap(id);

    person *current = table->bucket[index];
    person *prev = NULL;

    while (current != NULL) {
        if (strcmp(current->id, id) == 0) {
            if (prev == NULL) {
                table->bucket[index] = current->next;
            } else {
                prev->next = current->next;
            }

            free(current);
            return;
        }

        prev = current;
        current = current->next;
    }
    printf("No person found with id: %s\n", id);
}

void searchData(HashTable *table, char *id) {   
    unsigned int index = hashMap(id);

    person *current = table->bucket[index];

    while (current != NULL) {
        if (strcmp(current->id, id) == 0) {
            printf("Person found: \n");
            printf("Name: %s\n", current->name);
            printf("ID: %s\n", current->id);
            printf("Age: %d\n", current->age);
            printf("Gender: %s\n", current->gender);
            return;
        }
        current = current->next;
    }

    printf("No person found with id: %s\n", id);
}


void menu(person **head, person **tail, int userOpt, HashTable *table, char *id) {
    switch (userOpt) {
    case 1:
        addData(table);
        break;
    case 2:
        deleteData(table, id);
        break;
    case 3:
        searchData(head, tail);
        break;
    case 4:
        printf("Exiting the program....\n");
        break;
    default:
        printf("Invalid input\n");
        break;
    }
}

int main(void) {
    int userOpt;
    person *head = NULL;
    person *tail = NULL;
    HashTable table;


    do {
#ifdef _WIN32
        system("cls");
#else
        system("clear");
#endif

        printf("This is a hashMap\n");
        printf(" 1. Add a data\n");
        printf(" 2. Delete a data\n");
        printf(" 3. Search a data\n");
        printf("4. Exit\n");
        printf("=======================\n");
        scanf("%d", &userOpt);
        clearBufferInput();

        menu(&head, &tail, userOpt, &table, random_generated_id());

    } while (userOpt != 4);

    return 0;
}
