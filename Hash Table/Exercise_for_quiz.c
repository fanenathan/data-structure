#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TABLE_SIZE 31
#define generatedNUM [1000] = {0};

typedef struct Student {
    int NIM;
    char *name;
    char *age;

    struct Student *next;
    struct Student *prev;
} Student;

typedef struct HashMap {
    Student *slot[TABLE_SIZE];
} HashMap;

unsigned int hashMap(int NIM) {
    unsigned int val = 5381;
    int c;

    while (c = NIM++) {
        val = ((val << 5) + c);
        val &= 0xFFFFFFFF;
    }

    return val % TABLE_SIZE;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != "\n" && c != EOF) {
    }
}

void randomGenNIM() {
    srand(time(NULL));
}

void menu(Student **head, Student **tail, HashMap *table, )

    int main(void) {
    int userOpt;
    Student *head = NULL;
    Student *tail = NULL;
    HashMap table;

    do {
#ifdef _WIN32
        system("cls");
#else
        system("clear");
#endif

        printf("======= Menu =======\n");
        printf(" 1. Add data\n");
        printf(" 2. Delete data\n");
        printf(" 3. Search data\n");
        printf(" 4. Exit\n");
        printf("====================\n");
        printf("Option: ");
        scanf("%d", &userOpt);
        clearBufferInput();

        menu(&head, &tail, userOpt, &table);
    } while (userOpt != 4);

    return 0;
}