    #include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SIZE 50

typedef struct person {
    char *name;
    char *id;
    int age;
    const char *gender;

    struct person *next;
    struct person *prev;
} person;

typedef struct Hashmap {
    person *slot[SIZE];
} Hashmap;

unsigned int Hashing(char *id) {
    unsigned int val = 5381;
    int c;
    while ((c = *id++)) {
        val = ((val << 4) + c);
        val &= 0xFFFFFFFF;
    }
    return val % SIZE;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

char *read_input() {
    char *userInput = NULL;
    int c;
    size_t size = 0;

    while ((c = getchar()) != '\n' && c != EOF) {
        char *temp = (char *)realloc(userInput, size + 2);
        if (temp == NULL) {
            free(userInput);
            printf("Failed to allocated memories for user input\n");
            exit(1);
        }

        userInput = temp;
        userInput[size++] = c;
    }

    if (userInput != NULL) {
        userInput[size] = '\0';
    }

    return userInput;
}

char *name() {
    char *userInput;
    printf("Please enter your name: ");

    do {
        userInput = read_input();
        if (strlen(userInput) == 0) {
            printf("Please don't leave it blank!\n");
        }
    } while (strlen(userInput) == 0);

    return userInput;
}

int is_unique(int flag, char *userID[], char *ID) {
    for (int i = 0; i < flag; i++) {
        if (strcmp(userID[i], ID) == 0) return 0;
    }
    return 1;
}

char **userIDs = NULL;

char *id() {
    srand(time(NULL));

    char *userID = (char *)malloc(sizeof(char) * 11);
    if (userID == NULL) {
        printf("Failed to allocate memories for creating the memories size user ID\n");
        exit(1);
    }

    int flag = 0;

    do {
        int ID = (rand() % 90000000) + 10000000;
        ID = ID * 10 + (rand() % 10);
        sprintf(userID, "%010d", ID);
    } while (!is_unique(flag, userIDs, userID));

    userIDs = (char **)realloc(userIDs, (flag + 1) * sizeof(char *));

    if (userIDs == NULL) {
        printf("Failed to resize the memories for user ID\n");
        exit(1);
    }

    userIDs[flag++] = userID;

    return userID;
}

void gender_option(int userOpt, person *create) {
    switch (userOpt) {
        case 1:
            create->gender = "Male";
            break;
        case 2:
            create->gender = "Female";
            break;
        default:
            printf("Invalid gender option. Please select either Male or Female (NO NON-BINARY) mannnnn!\n");
            break;
    }
}

void gender(person *create) {
    int userOpt;

    printf("\n\nPlease select your gender: \n");
    printf("1. Male\n");
    printf("2. Female\n\n");
    printf("==========================\n");
    printf("No others okay!\n");
    printf("Option: ");
    scanf("%d", &userOpt);
    clearBufferInput();

    gender_option(userOpt, create);
}

int age() {
    int age;
    printf("Please enter the age: ");
    scanf("%d", &age);
    clearBufferInput();
    return age;
}

person *data(person **head, person **tail) {
    person *create = (person *)malloc(sizeof(person));

    if (create == NULL) {
        printf("Failed to allocate memories when creating a data\n");
        exit(1);
    }

    create->name = name();
    create->age = age();
    gender(create);
    create->id = id();

    create->next = NULL;
    create->prev = NULL;

    return create;
}

void addPerson(person ***head, person ***tail, Hashmap **table) {
    person *newPerson = data(*head, *tail);
    int index = Hashing(newPerson->id);

    newPerson->next = (*table)->slot[index];
    (*table)->slot[index] = newPerson;
}

void deletePerson(Hashmap **table, char *id) {
    int index = Hashing(id);
    person *temp = (*table)->slot[index];
    person *prev = NULL;

    // Traverse the list at this index;
    while (temp != NULL) {
        if (strcmp(temp->id, id) == 0) {
            if (prev == NULL) {
                (*table)->slot[index] = temp->next;
            } else {
                prev->next = temp->next;
            }

            free(temp->name);
            free(temp->id);
            free(temp);

            printf("Person with ID: %s has been removed\n", id);
            return;
        }

        prev = temp;
        temp = temp->next;
    }

    printf("The person [%s] you are searching for is not available.\n", id);
}

void menuFunction(int userOpt, person **head, person **tail, Hashmap **table) {
    switch (userOpt) {
        case 1:
            addPerson(&head, &tail, table);
            break;
        case 2: {
            char *id = (char *)malloc(sizeof(char) * 11);
            printf("Please enter the ID: ");
            fgets(id, 11, stdin);
            id[strcspn(id, "\n")] = '\0';
            deletePerson(table, id);
            clearBufferInput();
            printf("\n\nPress enter to continue....");
            getchar();
            break;
        }
        case 3:
            // searchPerson(table);
            break;
        case 4:
            printf("Exiting the program....\n");
            break;
        default:
            printf("Invalid input please try again.\n");
            break;
    }
}

void display(Hashmap **table) {
    int data_exists = 0;
    printf("----------------------------------------------------\n");
    printf("| %-10s | %-20s | %-3s | %-6s |\n", "ID", "Name", "Age", "Gender");
    printf("----------------------------------------------------\n");

    for (int i = 0; i < SIZE; i++) {
        person *temp = (*table)->slot[i];
        while (temp != NULL) {
            printf("| %-10s | %-20s | %-3d | %-6s |\n", temp->id, temp->name, temp->age, temp->gender);
            temp = temp->next;
            data_exists++;
        }
    }

    if (!data_exists) {
        printf("|                    %-29s |\n", "NO DATA");
    }

    printf("----------------------------------------------------\n");
}

int main(void) {
    int userOpt;
    person *head = NULL;
    person *tail = NULL;

    Hashmap *table = (Hashmap *)malloc(sizeof(Hashmap));

    do {
#ifdef _WIN32
        system("cls");
#else
        system("clear");
#endif
        display(&table);
        printf("\n======== Menu ========\n");
        printf(" 1. Add person\n");
        printf(" 2. Delete a person\n");
        printf(" 3. Search a person\n");
        printf(" 4. Exit\n");
        printf("======================\n");
        printf("Option: ");
        scanf("%d", &userOpt);
        clearBufferInput();

        menuFunction(userOpt, &head, &tail, &table);
    } while (userOpt != 4);

    return 0;
}