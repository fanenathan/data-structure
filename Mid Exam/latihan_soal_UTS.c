#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SIZE 100

typedef struct Node {
    char *userName;
    char *roomType;
    char *bookID;
    char *phoneNum;
    int days;

    struct Node *next;
    struct Node *prev;
} Node;

typedef struct Hashmap {
    Node *slot[SIZE];
} Hashmap;

// Hashing technique right HERE!!!
int Hashing(Node *bookingID) {
    int val = strlen(bookingID->bookID);

    if (val < 3) {
        printf("Booking ID must have at least 3 digits.\n");
        return -1;
    }

    int x = (bookingID->bookID[val - 3] - '0') + (bookingID->bookID[val - 2] - '0') + (bookingID->bookID[val - 1] - '0') - 1;

    int key = x % SIZE;

    return key;
}

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

char *name() {
    char *userName = NULL;
    size_t size = 0;
    int c;

    printf("Please enter your name: ");
    while ((c = getchar()) != '\n' && c != EOF) {
        char *Name = (char *)realloc(userName, size + 1);

        if (!name) {
            free(userName);
            printf("Failed to allocate memories for user input\n");
            exit(1);
        }

        userName = Name;
        userName[size++] = c;
    }

    char *Name = realloc(userName, size + 1);
    if (!name) {
        free(userName);
        printf("Failed to allocate memories for user input\n");
        exit(1);
    }

    userName = Name;
    userName[size] = '\0';

    return userName;
}

char *roomType() {
    char *room = (char *)malloc(sizeof(char) * 11);
    if (room == NULL) {
        printf("Failed to allocate memories for room type\n");
        exit(1);
    }

    do {
        printf("\n==== Room Types ====\n");
        printf("| 1. Regular       |\n");
        printf("| 2. Deluxe        |\n");
        printf("| 3. Suite         |\n");
        printf("====================\n");
        printf("(Case sensitive)\n");
        printf(">> ");
        fgets(room, 11, stdin);
        room[strcspn(room, "\n")] = '\0';

        if (strcmp(room, "Regular") && strcmp(room, "Deluxe") && strcmp(room, "Suite")) {
            printf("Room type must be either Regular / Deluxe / Suite!\n");
        }
    } while (strcmp(room, "Regular") && strcmp(room, "Deluxe") && strcmp(room, "Suite"));

    return room;
}

char *randID();

char *bookID(char *roomType, Node *booking) {
    char *newID = randID();

    char *finalBookingID = (char *)malloc(strlen(roomType) + strlen(newID) + 1);
    if (finalBookingID == NULL) {
        printf("Failed to allocate memories for the final booking ID\n");
        exit(1);
    }

    strcpy(finalBookingID, roomType);
    strcat(finalBookingID, newID);

    booking->bookID = finalBookingID;

    free(newID);

    return finalBookingID;
}

char *phoneNum() {
    char *phone = (char *)malloc(sizeof(char) * 15);
    if (phone == NULL) {
        printf("Failed to allocate memories for user phone number\n");
        exit(1);
    }

    do {
        printf("Input phone number: ");
        fgets(phone, 15, stdin);
        phone[strcspn(phone, "\n")] = '\0';

        if (strncmp(phone, "+62 ", 4) != 0) {
            printf("Phone number must start with '+62 '!\n");
        }
    } while (strncmp(phone, "+62 ", 4) != 0);

    return phone;
}

int days() {
    int day;
    printf("How long do you want to stay?\n");
    printf("[Only able to book for 1 - 30 days]\n");
    do {
        printf("Days: ");
        scanf("%d", &day);
        if (day < 1 || day > 30) {
            printf("You can't stay less then 1 day or nore than 30 days!\n");
        }
    } while (day < 1 || day > 30);

    return day;
}

int age() {
    int opt;
    printf("Please verify that you are more than 18+: \n");
    printf("1. Yes\n");
    printf("2. No\n");
    scanf("%d", &opt);
    clearBufferInput();

    int age;

    if (opt == 1) {
        printf("Please enter your age: ");
        scanf("%d", &age);
        clearBufferInput();
        return age;
    }
    return 0;
}

int isUnique(int flag, char *ID, char **generatedIDs) {
    for (int i = 0; i < flag; i++) {
        if (strcmp(generatedIDs[i], ID) == 0) return 0;
    }
    return 1;
}

char **generatedIDs = NULL;

char *randID() {
    srand(time(NULL));
    char *ID = (char *)malloc(sizeof(char) * 4);
    if (ID == NULL) {
        printf("Failed to allocate memories for ID\n");
        exit(1);
    }

    int flag = 0;

    do {
        int generateID = rand() % 1000;
        sprintf(ID, "%03d", generateID);
    } while (!isUnique(flag, ID, generatedIDs));

    generatedIDs = (char **)realloc(generatedIDs, (flag + 1) * sizeof(char *));
    if (generatedIDs == NULL) {
        printf("Failed to copy memories from generateID to generatedIDs\n");
        exit(1);
    }

    generatedIDs[flag] = (char *)malloc(sizeof(char) * 4);
    strcpy(generatedIDs[flag], ID);
    flag++;

    return ID;
}

Node *room(Node **head, Node **tail) {
    Node *create = (Node *)malloc(sizeof(Node));

    if (create == NULL) {
        printf("Failed to allocate memory for creating a linked list\n");
        exit(1);
    }

    create->userName = name();
    create->phoneNum = phoneNum();
    int valid = age();
    if (valid < 18) {
        printf("You are not old enough to book the hotels.\n");
        exit(1);
    }
    create->roomType = roomType();
    create->bookID = randID();
    create->days = days();

    return create;
}

void createBooking(Node ***head, Node ***tail, Hashmap **table) {
    Node *booking = room(*head, *tail);
    int index = Hashing(booking);

    booking->next = (*table)->slot[index];
    (*table)->slot[index] = booking;
}

void menu(int userOpt, Node **head, Node **tail, Hashmap **table) {
    switch (userOpt) {
        case 1:
            createBooking(&head, &tail, table);
            break;
        case 2:
            // viewBooking(&table);
            break;
        case 3:
            // deleteBooking(&table);
            break;
        case 4:
            printf("Exiting.....\n");
            break;
        default:
            printf("Invalid input please enter the right option!\n");
            break;
    }
}

int main(void) {
    Node *head = NULL;
    Node *tail = NULL;
    Hashmap *table = (Hashmap *)malloc(sizeof(Hashmap));
    int userOpt;

    do {
#ifdef _WIN32
        system("cls");
#else
        // system("clear");
#endif

        printf("+--------------------+\n");
        printf("| Hotel GrAnsylvania |\n");
        printf("+--------------------+\n");
        printf("\n1. Booking Hotel\n");
        printf("2. View Bookings\n");
        printf("3. Delete Booking\n");
        printf("4. Exit\n");
        printf(">> ");
        scanf("%d", &userOpt);
        clearBufferInput();

        menu(userOpt, &head, &tail, &table);

    } while (userOpt != 4);

    return 0;
}