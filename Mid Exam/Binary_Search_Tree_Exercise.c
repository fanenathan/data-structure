#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct Node {
    int key;
    struct Node *left;
    struct Node *right;
};

void clearBufferInput() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
    }
}

struct Node *insert(struct Node *root, int key) {
    if (root == NULL) {
        struct Node *new = (struct Node *)malloc(sizeof(struct Node));
        if (new == NULL) {
            printf("Memory allocation failed\n");
            exit(EXIT_FAILURE);
        }
        new->key = key;
        new->left = new->right = NULL;
        return new;
    }
    if (key < root->key) {
        root->left = insert(root->left, key);
    } else if (key > root->key) {
        root->right = insert(root->right, key);
    }

    return root;
}

struct Node *minValueNode(struct Node *node) {
    struct Node *current = node;
    while (current && current->right != NULL) {
        current = current->right;
    }
    return current;
}

struct Node *deleteNode(struct Node *root, int key) {
    if (root == NULL) {
        printf("The data is not available or not found\n");
        return root;
    }
    if (key < root->key) {
        root->left = deleteNode(root->left, key);
    } else if (key > root->key) {
        root->right = deleteNode(root->right, key);
    } else {
        if (root->left == NULL) {
            struct Node *temp = root->right;
            free(root);
            return temp;
        } else if (root->right == NULL) {
            struct Node *temp = root->left;
            free(root);
            return temp;
        }
        struct Node *temp = minValueNode(root->right);
        root->key = temp->key;
        root->right = deleteNode(root->right, temp->key);
    }

    return root;
}

void delete(struct Node **root, int key) {
    *root = deleteNode(*root, key);
}

int main(void) {
    struct Node *root = NULL;
    int option, key;

    do {
        printf("This is a BST program hopefully you like it\n");
        printf("1. Insert\n");
        printf("2. Delete\n");
        printf("3. Search\n");
        printf("4. Exit\n");
        scanf("%d", &option);
        clearBufferInput();

        switch (option) {
            case 1:
                printf("Enter key to insert: ");
                scanf("%d", &key);
                root = insert(root, key);
                clearBufferInput();
                break;
            case 2:
                printf("Enter the key you want to delete: ");
                scanf("%d", &key);
                delete (&root, key);
                clearBufferInput();
                break;
            case 3:
                // search(root);
                break;
            case 4:
                printf("Exiting\n");
                break;
            default:
                printf("Invalid option\n");
                break;
        }
    } while (option != 4);

    return 0;
}